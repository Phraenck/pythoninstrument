import numpy as np

class Note:
    """
    Represents an individual music note
    """

    def __init__(self, noteFreq):
        """
        Initialize a new note

        noteFreq - frequency of the note (in hertz)
        """

        fs = 44100       # sampling rate, Hz, must be integer
        duration = 0.5   # in seconds, may be float
        self.sineWave = (np.sin(((2 * np.pi * (np.arange((fs / noteFreq) * 100)) * noteFreq) / fs))).astype(np.float32)

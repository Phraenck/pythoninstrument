import keyboard
import Instrument

class PlayInstrument:
    """
    Play an Instrument.Instrument
    Handles receiving keyboard input from the user and passing it to the Instrument.
    """

    def __init__(self):
        """
        Initialize the instrument and set the keymap (dvorak)
        """
        self._instrument = Instrument.Instrument()

        self._keymap = ['1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0', 
                '\'',  ',',  '.',  'p',  'y',  'f',  'g',  'c',  'r',  'l', 
                'a',  'o',  'e',  'u',  'i',  'd',  'h',  't',  'n',  's', 
                ';',  'q',  'j',  'k',  'x',  'b',  'm',  'w',  'v',  'z']

    def startLoop(self):
        """
        Start the main loop.  Continue playing until the escape key is pressed.
        """
        while not keyboard.is_pressed("escape"):
            pressedKeys = map(lambda x: x if keyboard.is_pressed(x) else None, self._keymap) #create a list of keys that are pressed (None if the key isn't pressed)
            pressedKeys = list(filter(lambda x: x is not None, pressedKeys)) #filter out the Nones
            self._instrument.playNotes(pressedKeys) #play the note(s)


# start the program
if __name__ == "__main__":
    playInstrument = PlayInstrument()
    playInstrument.startLoop()

import Note
import InstrumentString as InsStr 
import numpy as np
import pyaudio

class Instrument:
    """
    Represents a 4 stringed musical instrument tuned with G-D-A-E tuning that plays notes based on keyboard input.
    """
    
    def __init__(self):
        """
        Initialize new instrument
        """
        self._notes = [
                 Note.Note(196.0),   # 'G3'
                 Note.Note(220.0),   # 'A3'
                 Note.Note(246.94),  # 'B3'
                 Note.Note(261.63),  # 'C4'
                 Note.Note(293.66),  # 'D4'
                 Note.Note(329.63),  # 'E4'
                 Note.Note(349.23),  # 'F4'
                 Note.Note(392.0),   # 'G4'
                 Note.Note(440.0),   # 'A4'
                 Note.Note(493.88),  # 'B4'
                 Note.Note(523.25),  # 'C5'
                 Note.Note(587.33),  # 'D5'
                 Note.Note(659.25),  # 'E5'
                 Note.Note(698.46),  # 'F5'
                 Note.Note(783.99),  # 'G5'
                 Note.Note(880.0),   # 'A5'
                 Note.Note(987.77),  # 'B5'
                 Note.Note(1046.5),  # 'C6'
                 Note.Note(1174.66), # 'D6'
                 Note.Note(1318.51), # 'E6'
                 Note.Note(1396.91), # 'F6'
                 Note.Note(1567.98)  # 'G6'
        ]

        #use G-D-A-E tuning.  Map the top row (number row) to the E-string and the bottom row to the G-string
        self._strings = [InsStr.InstrumentString(self._notes[0 : 10], [';', 'q', 'j', 'k', 'x', 'b', 'm', 'w', 'v', 'z']),
                InsStr.InstrumentString(self._notes[4 : 14], ['a', 'o', 'e', 'u', 'i', 'd', 'h', 't', 'n', 's']),
                InsStr.InstrumentString(self._notes[8 : 18], ['\'', ',', '.', 'p', 'y', 'f', 'g', 'c', 'r', 'l']),
                InsStr.InstrumentString(self._notes[12 : 22], ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'])]

        #initialize the audio stream
        self._audioStream = pyaudio.PyAudio();
        self._stream = self._audioStream.open(format = pyaudio.paFloat32,
                channels = 1,
                rate = 44100,
                output = True)

    def playNotes(self, keys):
        """
        Given a list of keys that are pressed on the keyboard, play the notes that correspond with the keys.

        keys - list of keys currently pressed on the keyboard (e.g. ['a', 'q', '4'])
        """

        if len(keys) == 0:
            return

        #get the sine waves for each note
        selectedNotes = []
        for string in self._strings:
            for key in keys:
                note = string.getNoteSine(key)
                if note is not None:
                    selectedNotes.append(note)

        #if multiple notes are selected, sum them to form a chord
        chord = None
        for note in selectedNotes:
            if chord is None:
                chord = note.copy()
            else:
                #the length of a note is not constant.  The note's sine wave will be 100 periods of its frequency.
                #Because of this, only sum up until the length of the shortest note.
                minLen = min(len(chord), len(note))
                chord[0 : minLen] = chord[0 : minLen] + note[0 : minLen]

        #scale the volume of the combined signals
        chord = (1 / len(selectedNotes)) * chord

        #play the notes
        self._stream.write(chord)

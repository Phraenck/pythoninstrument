class InstrumentString:
    """
    Represents a string on an instrument.  A string has notes that can be played.  Each note is mapped to a key.
    """

    def __init__(self, notes, keymap):
        """
        Initialize the string

        notes - An array of Note.Note objects
        keymap - An array of characters that corresponds with they key on the keyboard that is mapped to the note
        """
        self._notes = notes
        self._keymap = keymap

    def getNoteSine(self, key):
        """
        Get Note Sine
        Get the sine wave for a note on the string for a particular key.
        If the key is not on the keymap, return None
        """
        return self._notes[self._keymap.index(key)].sineWave if key in self._keymap else None
